%global commit 5bc3db730f490e6c3c12e1f83d1286ed2c017d5c
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Summary:       Microsoft Visio documents (VSS and VSD) to SVG conversion library
Name:          libvisio2svg
Version:       0.5.5
Release:       3.20190202git%{shortcommit}%{?dist}
License:       GPLv2+
Source:        https://github.com/kakwa/%{name}/archive/%{commit}/%{name}-%{shortcommit}.tar.gz
URL:           https://github.com/kakwa/libvisio2svg
BuildRequires: cmake
BuildRequires: gcc-c++
BuildRequires: libemf2svg-devel
BuildRequires: librevenge-devel
BuildRequires: libvisio-devel
BuildRequires: libwmf-devel
Requires:      libemf2svg

%description
Library/Tools to convert Microsoft Visio documents (VSS and VSD) to SVG

%package devel
Summary:       Development files for libvisio2svg
Requires:      %{name}%{?_isa} = %{version}-%{release}

%description devel
The libvisio2svg-devel package contains header files and documentation necessary
for developing programs using the visio2svg library.

%prep
%autosetup -n %{name}-%{commit}

%build
%cmake .
%cmake_build

%install
%cmake_install

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%license LICENSE
%doc README.md
%{_bindir}/vsd2svg-conv
%{_bindir}/vss2svg-conv
%{_libdir}/libTitleGenerator.so.0
%{_libdir}/libTitleGenerator.so.%{version}
%{_libdir}/libVisio2Svg.so.0
%{_libdir}/libVisio2Svg.so.%{version}

%files devel
%{_libdir}/libVisio2Svg.so
%{_libdir}/libTitleGenerator.so
%{_includedir}/visio2svg

%changelog
* Sat Feb 2 2019 spike <spike@fedoraproject.org> 0.5.5-3.20190202git5bc3db7
- Updated to latest git commit

* Sun May 13 2018 spike <spike@fedoraproject.org> 0.5.5-2.20180513gitd69aa12
- Updated to latest git commit

* Tue Apr 24 2018 spike <spike@fedoraproject.org> 0.5.5-1.20180308git586a622
- Updated to 0.5.5 release

* Thu Nov 30 2017 spike <spike@fedoraproject.org> 0.5.4-1.20171130git8c05dd0
- Initial package
